#!/bin/bash

#Usage: ./nucOccurances.sh <A or T or G or C> <Output_prefix> R1.sam Read_Length errorMatrix_A_new.txt errorMatrix_T_new.txt errorMatrix_G_new.txt errorMatrix_C_new.txt

NUC=$1
PREFIX=$2
FILE_fwd=$3
FILE_rev=$4
length=$5
eMatrix_A=$6
eMatrix_T=$7
eMatrix_G=$8
eMatrix_C=$9

nr_lines=$length

case "$NUC" in
"A")
	for ((i=1; i<$nr_lines+1; i++))
	do
		A_reads_1=$(awk '{print substr ($1,'$i',1)}' $FILE_fwd | grep -c "A")
		#echo $A_reads_1
		A_reads=$(( $A_reads_1+$(awk '{print substr ($1,'$length+1-$i',1)}' $FILE_rev | grep -c "T") ))
		#echo $A_reads	
		A_subs=$(awk 'FNR == '$i' {print $2+$3+$4}' $eMatrix_A)	
		#echo $A_subs
		T_subs=$(awk 'FNR == '$i' {print $1}' $eMatrix_T)
		#echo $T_subs
		G_subs=$(awk 'FNR == '$i' {print $1}' $eMatrix_G)
		#echo $G_subs
		C_subs=$(awk 'FNR == '$i' {print $1}' $eMatrix_C)
		#echo $C_subs
		let "sum=$A_reads+$A_subs-$T_subs-$G_subs-$C_subs"
		#echo ">>>>>".$sum
		echo $sum >> $PREFIX.nr_A_posSpec_v0.5.txt
	done
	;;
"T")
	for ((i=1; i<$nr_lines+1; i++))
	do
		T_reads=0
		T_reads_1=0
		T_reads_1=$(awk '{print substr ($1,'$i',1)}' $FILE_fwd | grep -c "T")
		#echo $T_reads_1
		T_reads=$(( $T_reads_1+$(awk '{print substr ($1,'$length+1-$i',1)}' $FILE_rev | grep -c "A") ))
		#echo "i="$i " length="$length
		#echo $(awk '{print substr ($1,'$length-$i',1)}' R1_rev.txt | grep -c "A")
		#echo "="$T_reads
		T_subs=$(awk 'FNR == '$i' {print $1+$3+$4}' $eMatrix_T)	
		#echo $T_subs
		A_subs=$(awk 'FNR == '$i' {print $2}' $eMatrix_A)
		#echo $A_subs
		G_subs=$(awk 'FNR == '$i' {print $2}' $eMatrix_G)
		#echo $G_subs
		C_subs=$(awk 'FNR == '$i' {print $2}' $eMatrix_C)
		#echo $C_subs
		let "sum=$T_reads+$T_subs-$A_subs-$G_subs-$C_subs"
		#echo ">>>>>>>"$sum
		echo $sum >> $PREFIX.nr_T_posSpec_v0.5.txt
	done
	;;
"G")
	for ((i=1; i<$nr_lines+1; i++))
	do
		G_reads_1=$(awk '{print substr ($1,'$i',1)}' $FILE_fwd | grep -c "G")
		G_reads=$(( $G_reads_1+$(awk '{print substr ($1,'$length+1-$i',1)}' $FILE_rev | grep -c "C") ))
		G_subs=$(awk 'FNR == '$i' {print $1+$2+$4}' $eMatrix_G)	
		A_subs=$(awk 'FNR == '$i' {print $3}' $eMatrix_A)
		T_subs=$(awk 'FNR == '$i' {print $3}' $eMatrix_T)
		C_subs=$(awk 'FNR == '$i' {print $3}' $eMatrix_C)
		
		let "sum=$G_reads+$G_subs-$A_subs-$T_subs-$C_subs"
		echo $sum >> $PREFIX.nr_G_posSpec_v0.5.txt
	done
	;;
"C")
	for ((i=1; i<$nr_lines+1; i++))
	do
		C_reads_1=$(awk '{print substr ($1,'$i',1)}' $FILE_fwd | grep -c "C")
		C_reads=$(( $C_reads_1+$(awk '{print substr ($1,'$length+1-$i',1)}' $FILE_rev | grep -c "G") ))
		C_subs=$(awk 'FNR == '$i' {print $1+$2+$3}' $eMatrix_C)	
		A_subs=$(awk 'FNR == '$i' {print $4}' $eMatrix_A)
		T_subs=$(awk 'FNR == '$i' {print $4}' $eMatrix_T)
		G_subs=$(awk 'FNR == '$i' {print $4}' $eMatrix_G)
		
		let "sum=$C_reads+$C_subs-$A_subs-$T_subs-$G_subs"
		echo $sum >> $PREFIX.nr_C_posSpec_v0.5.txt
	done
	;;
*)
	echo Invalid input option
	exit1
	;;
esac


#!/bin/bash

#Last update 05.01.2015
#Author: Melanie Schirmer, University of Glasgow, mail@melanieschirmer.com

###########################
#License: FreeBSD
#
#Copyright (c) 2015, Melanie Schirmer.
#
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met: 
#
#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer. 
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution. 
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#The views and conclusions contained in the software and documentation are those
#of the authors and should not be interpreted as representing official policies, 
#either expressed or implied, of the FreeBSD Project.
#
#########################


if [ "$1" == "-h" ] || [ "$1" == "" ]; then
  echo "

Usage: `basename $0` <Output_Prefix> <NO_Cores> <file.Calmd> <file.sam> <max_read_length>

The programme calculates position and nucleotide-specific error profiles for amplicon datasets
and requires the following input parameters:
    - A prefix for the output files (no whitespaces in prefix). A directory with the same name
      will be created in which the output files are saved. 
    - The number of cores that should be used for the computations.
    - A file with the aligned reads and extended MD tag.
    - A file in sam format with the aligned reads.
    - The maximum read length (e.g. the original length of the sequenced reads).

For additional information and an example see tutorial.  

Additional options:
    -h  shows this help text

Citation:
Melanie Schirmer, Umer Z. Ijaz, Rosalinda D'Amore, Neil Hall, William T. Sloan and Christopher Quince. Insight into biases and sequencing errors for amplicon sequencing with the Illumina MiSeq platform. Nucleic Acids Research, 2015.

If you would like to report a bug, please contact: mail@melanieschirmer.com

"

  exit 0
fi



startTime=`date +%s`

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PREFIX=$1
#echo $PREFIX
CORES=$2
#echo $CORES
SAMCALMD=$3
SAM=$4
ReadLength=$5

#Create directory for computations

if [ -d  "$PREFIX" ]; then
        echo Directory already exits
        exit 0
fi

mkdir $PREFIX
cd $PREFIX


#Compute read length
#ReadLength=$(sed '/^@/d' $SAMCALMD | head -1 | awk '{print $10}' | awk '{print length}') 

#echo -e '\n'The read length is $ReadLength


#Separate samCalmd file into forward and reverse reads
awk '$2=="0" {print}' $SAMCALMD > $PREFIX.fwd.samCalmd
awk '$2=="16" {print}' $SAMCALMD > $PREFIX.rev.samCalmd

echo Compute error profiles ...

#Apply sid.pl to forward reads

echo -e '\n'Forward reads:'\n\n'

#Split file
tempFile=(TempFWD*)

i=0
while [[ -f ${tempFile[$i]} ]]
do 
	#echo ${tempFile[$i]}
	rm ${tempFile[$i]}
	i=$(( $i+1))
done


file_lines_fwd=$(cat $PREFIX.fwd.samCalmd | wc -l)
#echo $file_lines_fwd
split_lines_fwd=$(($file_lines_fwd/$CORES))
#echo $split_lines_fwd
split_lines_fwd=$(($split_lines_fwd+1))
#echo $split_lines_fwd
split -d --lines=$split_lines_fwd $PREFIX.fwd.samCalmd TempFWD

echo Start sid.v0.x.pl ...

if [ -e TempFWD00 ]; then
	ls TempFWD* | parallel -k --max-procs $CORES $DIR/sid_qualScores_v1.0.pl {} $ReadLength {}.fwd_errorA.txt {}.fwd_errorT.txt {}.fwd_errorG.txt {}.fwd_errorC.txt {}.fwd_errorIns.txt {}.fwd_errorDel.txt {}.fwd_errorUnknown.txt {}.fwd_qualA.txt {}.fwd_qualT.txt {}.fwd_qualG.txt {}.fwd_qualC.txt {}.fwd_qualIns.txt {}.fwd_qualDel.txt {}.fwd_qualUnknown.txt
fi

echo Done with sid_v0.x.pl


if [ -f "HelpMatrixA.txt" ]
then
	rm HelpMatrixA.txt
fi

if [ -f "HelpMatrixT.txt" ]
then
        rm HelpMatrixT.txt
fi

if [ -f "HelpMatrixG.txt" ]
then
        rm HelpMatrixG.txt
fi

if [ -f "HelpMatrixC.txt" ]
then
        rm HelpMatrixC.txt
fi
if [ -f "HelpMatrixIns.txt" ]
then
	rm HelpMatrixIns.txt
fi
if [ -f "HelpMatrixDel.txt" ]
then
        rm HelpMatrixDel.txt
fi
if [ -f "HelpMatrixN.txt" ]
then
        rm HelpMatrixUnknown.txt
fi

#For qual matrices
if [ -f "HelpMatrixqualA.txt" ]
then
        rm HelpMatrixqualA.txt
fi

if [ -f "HelpMatrixqualT.txt" ]
then
        rm HelpMatrixqualT.txt
fi

if [ -f "HelpMatrixqualG.txt" ]
then
        rm HelpMatrixqualG.txt
fi

if [ -f "HelpMatrixqualC.txt" ]
then
        rm HelpMatrixqualC.txt
fi
if [ -f "HelpMatrixqualIns.txt" ]
then
        rm HelpMatrixqualIns.txt
fi
if [ -f "HelpMatrixqualDel.txt" ]
then
        rm HelpMatrixqualDel.txt
fi
if [ -f "HelpMatrixqualN.txt" ]
then
        rm HelpMatrixqualUnknown.txt
fi



#echo Creating help matrices ...

#exit 0

for a in `seq 1 $ReadLength`; do 
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixA.txt
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixT.txt
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixG.txt
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixC.txt 
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixIns.txt
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixDel.txt
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixUnknown.txt
	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> IfNoFwd.txt
	#qual matrices
	echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualA.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualT.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualG.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualC.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualIns.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualDel.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualUnknown.txt
        echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> IfNoFwdqual.txt
done	



filesFWD_A=(TempFWD*.fwd_errorA.txt)
filesFWD_T=(TempFWD*.fwd_errorT.txt)
filesFWD_G=(TempFWD*.fwd_errorG.txt)
filesFWD_C=(TempFWD*.fwd_errorC.txt)
filesFWD_Ins=(TempFWD*.fwd_errorIns.txt)
filesFWD_Del=(TempFWD*.fwd_errorDel.txt)
filesFWD_Unknown=(TempFWD*.fwd_errorUnknown.txt)
#qual files
filesFWD_qualA=(TempFWD*.fwd_qualA.txt)
filesFWD_qualT=(TempFWD*.fwd_qualT.txt)
filesFWD_qualG=(TempFWD*.fwd_qualG.txt)
filesFWD_qualC=(TempFWD*.fwd_qualC.txt)
filesFWD_qualIns=(TempFWD*.fwd_qualIns.txt)
filesFWD_qualDel=(TempFWD*.fwd_qualDel.txt)
filesFWD_qualUnknown=(TempFWD*.fwd_qualUnknown.txt)





#echo $filesFWD_A


#exit 0

for i in `seq 1 $CORES` ; do
	suffix=$(( $i-1 ))
	#echo $suffix

	if [[ (-e ${filesFWD_A[$suffix]}) ]] # && (-z ${filesFWD_A[$suffix]}) ]]
        then	
		#echo ${filesFWD_A[$suffix]}
		#echo "Adding A ..."
		$DIR/matrixAdd.pl $ReadLength 4 HelpMatrixA.txt ${filesFWD_A[$suffix]} HelpMatrixA.txt
	fi
	
	if [[ (-f ${filesFWD_T[$suffix]}) ]] #&& (-z ${filesFWD_T[$suffix]}) ]]
        then
		#echo "Adding T ..."
                $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixT.txt ${filesFWD_T[$suffix]} HelpMatrixT.txt
        fi

	if [[ (-f ${filesFWD_G[$suffix]}) ]] #&& (-z ${filesFWD_G[$suffix]}) ]]
        then
		#echo "Adding G ..."
                $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixG.txt ${filesFWD_G[$suffix]} HelpMatrixG.txt
        fi

	if [[ (-f ${filesFWD_C[$suffix]}) ]] #&& (-z ${filesFWD_C[$suffix]}) ]]
        then
		#echo "Adding C ..."
                $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixC.txt ${filesFWD_C[$suffix]} HelpMatrixC.txt
        fi
	
	if [[ (-f ${filesFWD_Ins[$suffix]}) ]] #&& (-z ${filesFWD_C[$suffix]}) ]]
        then
		#echo "Adding insertions ..."
                $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixIns.txt ${filesFWD_Ins[$suffix]} HelpMatrixIns.txt
        fi

	if [[ (-f ${filesFWD_Del[$suffix]}) ]] #&& (-z ${filesFWD_C[$suffix]}) ]]
        then
		#echo "Adding deletions ..."
                $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixDel.txt ${filesFWD_Del[$suffix]} HelpMatrixDel.txt
        fi

	if [[ (-f ${filesFWD_Unknown[$suffix]}) ]] #&& (-z ${filesFWD_C[$suffix]}) ]]
        then
		#echo "Adding N's ..."
                $DIR/matrixAdd.pl $ReadLength 1 HelpMatrixUnknown.txt ${filesFWD_Unknown[$suffix]} HelpMatrixUnknown.txt
        fi

	#qual
	if [[ (-e ${filesFWD_qualA[$suffix]}) ]] 
        then
                #echo ${filesFWD_qualA[$suffix]}
                #echo "Adding qual A ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualA.txt ${filesFWD_qualA[$suffix]} HelpMatrixqualA.txt
        fi

        if [[ (-f ${filesFWD_qualT[$suffix]}) ]] 
        then
                #echo "Adding qual T ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualT.txt ${filesFWD_qualT[$suffix]} HelpMatrixqualT.txt
        fi

        if [[ (-f ${filesFWD_qualG[$suffix]}) ]] 
        then
                #echo "Adding qual G ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualG.txt ${filesFWD_qualG[$suffix]} HelpMatrixqualG.txt
        fi

        if [[ (-f ${filesFWD_qualC[$suffix]}) ]] 
        then
                #echo "Adding qual C ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualC.txt ${filesFWD_qualC[$suffix]} HelpMatrixqualC.txt
        fi

        if [[ (-f ${filesFWD_qualIns[$suffix]}) ]] 
        then
                #echo "Adding qual insertions ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualIns.txt ${filesFWD_qualIns[$suffix]} HelpMatrixqualIns.txt
        fi

        if [[ (-f ${filesFWD_qualDel[$suffix]}) ]] 
        then
                #echo "Adding qual deletions ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualDel.txt ${filesFWD_qualDel[$suffix]} HelpMatrixqualDel.txt
        fi

        if [[ (-f ${filesFWD_qualUnknown[$suffix]}) ]] 
        then
                #echo "Adding qual N's ..."
                $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualUnknown.txt ${filesFWD_qualUnknown[$suffix]} HelpMatrixqualUnknown.txt
        fi

done

#exit 0

mv HelpMatrixA.txt $PREFIX.fwd_errorA.txt
mv HelpMatrixT.txt $PREFIX.fwd_errorT.txt
mv HelpMatrixG.txt $PREFIX.fwd_errorG.txt
mv HelpMatrixC.txt $PREFIX.fwd_errorC.txt 
mv HelpMatrixIns.txt $PREFIX.fwd_errorIns.txt
mv HelpMatrixDel.txt $PREFIX.fwd_errorDel.txt
mv HelpMatrixUnknown.txt $PREFIX.fwd_errorUnknown.txt
#qual matrices
mv HelpMatrixqualA.txt $PREFIX.fwd_qualA.txt
mv HelpMatrixqualT.txt $PREFIX.fwd_qualT.txt
mv HelpMatrixqualG.txt $PREFIX.fwd_qualG.txt
mv HelpMatrixqualC.txt $PREFIX.fwd_qualC.txt
mv HelpMatrixqualIns.txt $PREFIX.fwd_qualIns.txt
mv HelpMatrixqualDel.txt $PREFIX.fwd_qualDel.txt
mv HelpMatrixqualUnknown.txt $PREFIX.fwd_qualUnknown.txt





#exit 0

#Apply sid.pl to reverse reads

echo -e '\n'Reverse reads:'\n\n'

tempFile=(TempREV*)

i=0
while [[ -f ${tempFile[$i]}  ]]
do
        #echo ${tempFile[$i]}
        rm ${tempFile[$i]}
        i=$(( $i+1))
done



file_lines_rev=$(cat $PREFIX.rev.samCalmd | wc -l)
split_lines_rev=$(($file_lines_rev/$CORES))
split_lines_rev=$(($split_lines_rev+1))
split -d --lines=$split_lines_rev $PREFIX.rev.samCalmd TempREV

if [ -e TempREV00 ]; then
	ls TempREV* | parallel -k --max-procs $CORES $DIR/sid_qualScores_v1.0.pl {} $ReadLength {}.rev_errorA.txt {}.rev_errorT.txt {}.rev_errorG.txt {}.rev_errorC.txt {}.rev_errorIns.txt {}.rev_errorDel.txt {}.rev_errorUnknown.txt {}.rev_qualA.txt {}.rev_qualT.txt {}.rev_qualG.txt {}.rev_qualC.txt {}.rev_qualIns.txt {}.rev_qualDel.txt {}.rev_qualUnknown.txt
fi

if [ -f "HelpMatrixArev.txt" ]
then
        rm HelpMatrixArev.txt
fi
if [ -f "HelpMatrixTrev.txt" ]
then
        rm HelpMatrixTrev.txt
fi
if [ -f "HelpMatrixGrev.txt" ]
then
        rm HelpMatrixGrev.txt
fi

if [ -f "HelpMatrixCrev.txt" ]
then
        rm HelpMatrixCrev.txt
fi

if [ -f "HelpMatrixInsrev.txt" ]
then
        rm HelpMatrixInsrev.txt
fi

if [ -f "HelpMatrixDelrev.txt" ]
then
        rm HelpMatrixDelrev.txt
fi

if [ -f "HelpMatrixUnknownrev.txt" ]
then
        rm HelpMatrixUnknownrev.txt
fi

#qual matrices
if [ -f "HelpMatrixqualArev.txt" ]
then
        rm HelpMatrixqualArev.txt
fi
if [ -f "HelpMatrixqualTrev.txt" ]
then
        rm HelpMatrixqualTrev.txt
fi
if [ -f "HelpMatrixqualGrev.txt" ]
then
        rm HelpMatrixqualGrev.txt
fi

if [ -f "HelpMatrixqualCrev.txt" ]
then
        rm HelpMatrixqualCrev.txt
fi

if [ -f "HelpMatrixqualInsrev.txt" ]
then
        rm HelpMatrixqualInsrev.txt
fi

if [ -f "HelpMatrixqualDelrev.txt" ]
then
        rm HelpMatrixqualDelrev.txt
fi

if [ -f "HelpMatrixqualUnknownrev.txt" ]
then
        rm HelpMatrixqualUnknownrev.txt
fi



if [ -e TempREV00 ]; then
	for a in `seq 1 $ReadLength`; do
        	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixArev.txt
        	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixTrev.txt
        	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixGrev.txt
        	echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixCrev.txt
		echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixInsrev.txt
		echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixDelrev.txt
		echo -e "0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixUnknownrev.txt
		#qual scores
		echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualArev.txt
                echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualTrev.txt
                echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualGrev.txt
                echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualCrev.txt
                echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualInsrev.txt
                echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualDelrev.txt
                echo -e "0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0"'\t'"0" >> HelpMatrixqualUnknownrev.txt
	done


	filesREV_A=(TempREV*.rev_errorA.txt)
	filesREV_T=(TempREV*.rev_errorT.txt)
	filesREV_G=(TempREV*.rev_errorG.txt)
	filesREV_C=(TempREV*.rev_errorC.txt)
	filesREV_Ins=(TempREV*.rev_errorIns.txt)
	filesREV_Del=(TempREV*.rev_errorDel.txt)
	filesREV_Unknown=(TempREV*.rev_errorUnknown.txt)

	#qual matrices
	filesREV_qualA=(TempREV*.rev_qualA.txt)
        filesREV_qualT=(TempREV*.rev_qualT.txt)
        filesREV_qualG=(TempREV*.rev_qualG.txt)
        filesREV_qualC=(TempREV*.rev_qualC.txt)
        filesREV_qualIns=(TempREV*.rev_qualIns.txt)
        filesREV_qualDel=(TempREV*.rev_qualDel.txt)
        filesREV_qualUnknown=(TempREV*.rev_qualUnknown.txt)


	for i in `seq 1 $CORES` ; do
        
		#echo ${filesREV_A[$suffix]}	
	
		suffix=$(( $i-1 ))
        	if [[ (-e ${filesREV_A[$suffix]}) ]] #&& (-z ${filesREV_A[$suffix]}) ]]
        	then
       		         $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixArev.txt ${filesREV_A[$suffix]} HelpMatrixArev.txt
        	fi
        	if [[ (-e ${filesREV_T[$suffix]}) ]] #&& (-z ${filesREV_T[$suffix]}) ]]
        	then
        	        $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixTrev.txt ${filesREV_T[$suffix]} HelpMatrixTrev.txt
        	fi
        	if [[ (-e ${filesREV_G[$suffix]}) ]] #&& (-z ${filesREV_G[$suffix]}) ]]
        	then
                	$DIR/matrixAdd.pl $ReadLength 4 HelpMatrixGrev.txt ${filesREV_G[$suffix]} HelpMatrixGrev.txt
        	fi
        	if [[ (-e ${filesREV_C[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
        	then
        	        $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixCrev.txt ${filesREV_C[$suffix]} HelpMatrixCrev.txt
        	fi
		if [[ (-e ${filesREV_Ins[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
        	then
        	        $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixInsrev.txt ${filesREV_Ins[$suffix]} HelpMatrixInsrev.txt
        	fi
		if [[ (-e ${filesREV_Del[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
        	then
        	        $DIR/matrixAdd.pl $ReadLength 4 HelpMatrixDelrev.txt ${filesREV_Del[$suffix]} HelpMatrixDelrev.txt
        	fi
		if [[ (-e ${filesREV_Unknown[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
        	then
               	 	$DIR/matrixAdd.pl $ReadLength 1 HelpMatrixUnknownrev.txt ${filesREV_Unknown[$suffix]} HelpMatrixUnknownrev.txt
        	fi

		#qual scores
		if [[ (-e ${filesREV_qualA[$suffix]}) ]] #&& (-z ${filesREV_A[$suffix]}) ]]
                then
                         $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualArev.txt ${filesREV_qualA[$suffix]} HelpMatrixqualArev.txt
                fi
                if [[ (-e ${filesREV_qualT[$suffix]}) ]] #&& (-z ${filesREV_T[$suffix]}) ]]
                then
                        $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualTrev.txt ${filesREV_qualT[$suffix]} HelpMatrixqualTrev.txt
                fi
                if [[ (-e ${filesREV_qualG[$suffix]}) ]] #&& (-z ${filesREV_G[$suffix]}) ]]
                then
                        $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualGrev.txt ${filesREV_qualG[$suffix]} HelpMatrixqualGrev.txt
                fi
                if [[ (-e ${filesREV_qualC[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
                then
                        $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualCrev.txt ${filesREV_qualC[$suffix]} HelpMatrixqualCrev.txt
                fi
                if [[ (-e ${filesREV_qualIns[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
                then
                        $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualInsrev.txt ${filesREV_qualIns[$suffix]} HelpMatrixqualInsrev.txt
                fi
                if [[ (-e ${filesREV_qualDel[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
                then
                        $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualDelrev.txt ${filesREV_qualDel[$suffix]} HelpMatrixqualDelrev.txt
                fi
                if [[ (-e ${filesqualREV_Unknown[$suffix]}) ]] #&& (-z ${filesREV_C[$suffix]}) ]]
                then
                        $DIR/matrixAdd.pl $ReadLength 50 HelpMatrixqualUnknownrev.txt ${filesREV_qualUnknown[$suffix]} HelpMatrixqualUnknownrev.txt
                fi


	done

	#exit 0

        mv HelpMatrixArev.txt $PREFIX.rev_errorA.txt
        mv HelpMatrixTrev.txt $PREFIX.rev_errorT.txt
        mv HelpMatrixGrev.txt $PREFIX.rev_errorG.txt
        mv HelpMatrixCrev.txt $PREFIX.rev_errorC.txt
        mv HelpMatrixInsrev.txt $PREFIX.rev_errorIns.txt
        mv HelpMatrixDelrev.txt $PREFIX.rev_errorDel.txt
        mv HelpMatrixUnknownrev.txt $PREFIX.rev_errorUnknown.txt

	mv HelpMatrixqualArev.txt $PREFIX.rev_qualA.txt
	mv HelpMatrixqualTrev.txt $PREFIX.rev_qualT.txt
	mv HelpMatrixqualGrev.txt $PREFIX.rev_qualG.txt
	mv HelpMatrixqualCrev.txt $PREFIX.rev_qualC.txt
	mv HelpMatrixqualInsrev.txt $PREFIX.rev_qualIns.txt
	mv HelpMatrixqualDelrev.txt $PREFIX.rev_qualDel.txt
	mv HelpMatrixqualUnknownrev.txt $PREFIX.rev_qualUnknown.txt

fi

if [ -e TempREV00 ]; then
	if [ -e TempFWD00 ]; then
		#Reverse read direction of reverse reads and add forward reads
		$DIR/revOrder_errorMatrix.pl $ReadLength $PREFIX.rev_errorA.txt $PREFIX.rev_errorT.txt $PREFIX.rev_errorG.txt $PREFIX.rev_errorC.txt $PREFIX.rev_errorIns.txt $PREFIX.rev_errorDel.txt $PREFIX.rev_errorUnknown.txt $PREFIX.fwd_errorA.txt $PREFIX.fwd_errorT.txt $PREFIX.fwd_errorG.txt $PREFIX.fwd_errorC.txt $PREFIX.fwd_errorIns.txt $PREFIX.fwd_errorDel.txt $PREFIX.fwd_errorUnknown.txt $PREFIX.errorA.txt $PREFIX.errorT.txt $PREFIX.errorG.txt $PREFIX.errorC.txt $PREFIX.errorIns.txt $PREFIX.errorDel.txt $PREFIX.errorUnknown.txt
	else 
		$DIR/revOrder_errorMatrix.pl $ReadLength $PREFIX.rev_errorA.txt $PREFIX.rev_errorT.txt $PREFIX.rev_errorG.txt $PREFIX.rev_errorC.txt $PREFIX.rev_errorIns.txt $PREFIX.rev_errorDel.txt $PREFIX.rev_errorUnknown.txt IfNoFwd.txt IfNoFwd.txt IfNoFwd.txt IfNoFwd.txt IfNoFwd.txt IfNoFwd.txt IfNoFwd.txt $PREFIX.errorA.txt $PREFIX.errorT.txt $PREFIX.errorG.txt $PREFIX.errorC.txt $PREFIX.errorIns.txt $PREFIX.errorDel.txt $PREFIX.errorUnknown.txt
	fi
	if [ -e TempFWD00 ]; then
                #Reverse read direction of reverse reads and add forward reads
                $DIR/revOrder_qualMatrix.pl $ReadLength $PREFIX.rev_qualA.txt $PREFIX.rev_qualT.txt $PREFIX.rev_qualG.txt $PREFIX.rev_qualC.txt $PREFIX.rev_qualIns.txt $PREFIX.rev_qualDel.txt $PREFIX.rev_qualUnknown.txt $PREFIX.fwd_qualA.txt $PREFIX.fwd_qualT.txt $PREFIX.fwd_qualG.txt $PREFIX.fwd_qualC.txt $PREFIX.fwd_qualIns.txt $PREFIX.fwd_qualDel.txt $PREFIX.fwd_qualUnknown.txt $PREFIX.qualA.txt $PREFIX.qualT.txt $PREFIX.qualG.txt $PREFIX.qualC.txt $PREFIX.qualIns.txt $PREFIX.qualDel.txt $PREFIX.qualUnknown.txt
        else
                $DIR/revOrder_qualMatrix.pl $ReadLength $PREFIX.rev_qualA.txt $PREFIX.rev_qualT.txt $PREFIX.rev_qualG.txt $PREFIX.rev_qualC.txt $PREFIX.rev_qualIns.txt $PREFIX.rev_qualDel.txt $PREFIX.rev_qualUnknown.txt IfNoFwdqual.txt IfNoFwdqual.txt IfNoFwdqual.txt IfNoFwdqual.txt IfNoFwdqual.txt IfNoFwdqual.txt IfNoFwdqual.txt $PREFIX.qualA.txt $PREFIX.qualT.txt $PREFIX.qualG.txt $PREFIX.qualC.txt $PREFIX.qualIns.txt $PREFIX.qualDel.txt $PREFIX.qualUnknown.txt
        fi

else
	mv $PREFIX.fwd_errorA.txt $PREFIX.errorA.txt
	mv $PREFIX.fwd_errorT.txt $PREFIX.errorT.txt
	mv $PREFIX.fwd_errorG.txt $PREFIX.errorG.txt
	mv $PREFIX.fwd_errorC.txt $PREFIX.errorC.txt 
	mv $PREFIX.fwd_errorIns.txt $PREFIX.errorIns.txt
	mv $PREFIX.fwd_errorDel.txt $PREFIX.errorDel.txt
	mv $PREFIX.fwd_errorUnknown.txt $PREFIX.errorUnknown.txt
	#qual files
	mv $PREFIX.fwd_qualA.txt $PREFIX.qualA.txt
        mv $PREFIX.fwd_qualT.txt $PREFIX.qualT.txt
        mv $PREFIX.fwd_qualG.txt $PREFIX.qualG.txt
        mv $PREFIX.fwd_qualC.txt $PREFIX.qualC.txt
        mv $PREFIX.fwd_qualIns.txt $PREFIX.qualIns.txt
        mv $PREFIX.fwd_qualDel.txt $PREFIX.qualDel.txt
        mv $PREFIX.fwd_qualUnknown.txt $PREFIX.qualUnknown.txt
fi


#rm IfNoFwd.txt

#move intermediate files
mkdir IntermediateFiles 

if [ -e TempFWD00 ]; then
	mv $PREFIX.fwd.samCalmd IntermediateFiles
	mv TempFWD* IntermediateFiles
fi

if [ -e TempREV00 ]; then
	if [ -e TempFWD00 ]; then
		mv *$PREFIX.rev_error?.txt IntermediateFiles
		mv *$PREFIX.rev_qual?.txt IntermediateFiles
	fi
	mv $PREFIX.rev.samCalmd IntermediateFiles
	mv TempREV* IntermediateFiles
	mv *$PREFIX.fwd_error?.txt IntermediateFiles
	mv *$PREFIX.fwd_qual?.txt IntermediateFiles
fi

#exit 0

echo Compute nucleotide distribution ...

#nucleotide occurrences
if [ -e "$PREFIX.nr_A_posSpec_v0.5.txt" ]
then
rm $PREFIX.nr_A_posSpec_v0.5.txt
fi

if [ -e "$PREFIX.nr_T_posSpec_v0.5.txt" ]
then
rm $PREFIX.nr_T_posSpec_v0.5.txt
fi

if [ -e "$PREFIX.nr_G_posSpec_v0.5.txt" ]
then
rm $PREFIX.nr_G_posSpec_v0.5.txt
fi

if [ -e "$PREFIX.nr_C_posSpec_v0.5.txt" ]
then
rm $PREFIX.nr_C_posSpec_v0.5.txt
fi


echo A > $PREFIX.TempList.txt
echo T >> $PREFIX.TempList.txt
echo G >> $PREFIX.TempList.txt
echo C >> $PREFIX.TempList.txt

awk '$2=="0" {print $10}' $SAM > $PREFIX.fwd.txt
awk '$2=="16" {print $10}' $SAM > $PREFIX.rev.txt

cat $PREFIX.TempList.txt | parallel -k --max-procs 4 $DIR/nucOccurences_v1.0.sh {} $PREFIX $PREFIX.fwd.txt $PREFIX.rev.txt $ReadLength $PREFIX.errorA.txt $PREFIX.errorT.txt $PREFIX.errorG.txt $PREFIX.errorC.txt


mv $PREFIX.TempList.txt IntermediateFiles
mv $PREFIX.fwd.txt IntermediateFiles
mv $PREFIX.rev.txt IntermediateFiles
#mv $PREFIX.fwd_error* IntermediateFiles
#mv $PREFIX.rev_error* IntermediateFiles
mv IfNoFwd*.txt IntermediateFiles


#Plots

mkdir plots
cd plots

Rscript $DIR/ErrorPlots.R $PWD $PREFIX $ReadLength $PREFIX ../$PREFIX.errorA.txt ../$PREFIX.nr_A_posSpec_v0.5.txt ../$PREFIX.errorC.txt ../$PREFIX.nr_C_posSpec_v0.5.txt ../$PREFIX.errorG.txt ../$PREFIX.nr_G_posSpec_v0.5.txt ../$PREFIX.errorT.txt ../$PREFIX.nr_T_posSpec_v0.5.txt ../$PREFIX.errorIns.txt ../$PREFIX.errorDel.txt ../$PREFIX.errorUnknown.txt

Rscript $DIR/ErrorPlot_qual.R $PWD $PREFIX $PREFIX ../$PREFIX.qualA.txt ../$PREFIX.qualC.txt ../$PREFIX.qualG.txt ../$PREFIX.qualT.txt ../$PREFIX.qualIns.txt ../$PREFIX.qualDel.txt ../$PREFIX.qualUnknown.txt


cd ../..

echo Execution took $(expr `date +%s` - $startTime) seconds

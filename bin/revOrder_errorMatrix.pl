#!/usr/bin/perl

#Usage: ./revOrder_errorMatrix.pl Read_Length errorMatrix_A_rev errorMatrix_T_rev errorMatrix_G_rev errorMatrix_C_rev errorMatrix_A_fwd errorMatrix_T_fwd errorMatrix_G_fwd errorMatrix_C_fwd new_errorMatrix_A_all new_errorMatrix_T_all new_errorMatrix_G_all new_errorMatrix_C_all

use strict; use warnings; use diagnostics;

my $length1 = $ARGV[0];
my $errorA = $ARGV[1];
my $errorT = $ARGV[2];
my $errorG = $ARGV[3];
my $errorC = $ARGV[4];
my $errorIns = $ARGV[5];
my $errorDel = $ARGV[6];
my $errorUnknown = $ARGV[7];
my $errorA_fwd = $ARGV[8];
my $errorT_fwd = $ARGV[9];
my $errorG_fwd = $ARGV[10];
my $errorC_fwd = $ARGV[11];
my $errorIns_fwd = $ARGV[12];
my $errorDel_fwd = $ARGV[13];
my $errorUnknown_fwd = $ARGV[14];
my $new_errorA = $ARGV[15];
my $new_errorT = $ARGV[16];
my $new_errorG = $ARGV[17];
my $new_errorC = $ARGV[18];
my $new_errorIns = $ARGV[19];
my $new_errorDel = $ARGV[20];
my $new_errorUnknown = $ARGV[21];


open(FILE_A, $errorA) or die "Can't open A\n";
open(FILE_T, $errorT) or die "Can't open T\n";
open(FILE_G, $errorG) or die "Can't open G\n";
open(FILE_C, $errorC) or die "Can't open C\n";
open(FILE_Ins, $errorIns) or die "Can't open Ins\n";
open(FILE_Del, $errorDel) or die "Can't open Del\n";
open(FILE_Unknown, $errorUnknown) or die "Can't open Unknown\n";

open(FILE_A_fwd, $errorA_fwd) or die "Can't open A fwd\n";
open(FILE_T_fwd, $errorT_fwd) or die "Can't open T fwd\n";
open(FILE_G_fwd, $errorG_fwd) or die "Can't open G fwd\n";
open(FILE_C_fwd, $errorC_fwd) or die "Can't open C fwd\n";
open(FILE_Ins_fwd, $errorIns_fwd) or die "Can't open Ins fwd\n";
open(FILE_Del_fwd, $errorDel_fwd) or die "Can't open Del fwd\n";
open(FILE_Unknown_fwd, $errorUnknown_fwd) or die "Can't open Unknown fwd\n";

open(my $FILE_A_all, '>', $new_errorA) or die "Can't open errorMatrix_A_rev.txt\n";
open(my $FILE_T_all, '>', $new_errorT)	or die "Can't open errorMatrix_T_rev.txt\n";
open(my $FILE_G_all, '>', $new_errorG)	or die "Can't open errorMatrix_G_rev.txt\n";
open(my $FILE_C_all, '>', $new_errorC)	or die "Can't open errorMatrix_C_rev.txt\n";
open(my $FILE_Ins_all, '>', $new_errorIns)  or die "Can't open errorMatrix_Ins_rev.txt\n";
open(my $FILE_Del_all, '>', $new_errorDel)  or die "Can't open errorMatrix_Del_rev.txt\n";
open(my $FILE_Unknown_all, '>', $new_errorUnknown)  or die "Can't open errorMatrix_Unknown_rev.txt\n";


my @subA = (0,0,0,0);
my @subT = (0,0,0,0);
my @subG = (0,0,0,0);
my @subC = (0,0,0,0);
my @subIns = (0,0,0,0);
my @subDel = (0,0,0,0);
my @subUnknown = (0);

my @A = (\@subA);
my @T = (\@subT);
my @G = (\@subG);
my @C = (\@subC);
my @Ins = (\@subIns);
my @Del = (\@subDel);
my @Unknown = (\@subUnknown);

for (my $j=1;$j<$length1;$j++){
	#print "$j\n";
	my @subA = (0,0,0,0);
	push @A, \@subA;
	my @subT = (0,0,0,0);
	push @T, \@subT;
	my @subG = (0,0,0,0);
	push @G, \@subG;
	my @subC = (0,0,0,0);
	push @C, \@subC;
	my @subIns = (0,0,0,0);
        push @Ins, \@subIns;
	my @subDel = (0,0,0,0);
        push @Del, \@subDel;
	my @subUnknown = (0);
        push @Unknown, \@subUnknown;
}

my @subA_fwd = (0,0,0,0);
my @subT_fwd = (0,0,0,0);
my @subG_fwd = (0,0,0,0);
my @subC_fwd = (0,0,0,0);
my @subIns_fwd = (0,0,0,0);
my @subDel_fwd = (0,0,0,0);
my @subUnknown_fwd = (0);

my @A_fwd = (\@subA_fwd);
my @T_fwd = (\@subT_fwd);
my @G_fwd = (\@subG_fwd);
my @C_fwd = (\@subC_fwd);
my @Ins_fwd = (\@subIns_fwd);
my @Del_fwd = (\@subDel_fwd);
my @Unknown_fwd = (\@subUnknown_fwd);

for (my $j=1;$j<$length1;$j++){
        #print "$j\n";
        my @subA_fwd = (0,0,0,0);
        push @A_fwd, \@subA_fwd;
        my @subT_fwd = (0,0,0,0);
        push @T_fwd, \@subT_fwd;
        my @subG_fwd = (0,0,0,0);
        push @G_fwd, \@subG_fwd;
        my @subC_fwd = (0,0,0,0);
        push @C_fwd, \@subC_fwd;
	my @subIns_fwd = (0,0,0,0);
        push @Ins_fwd, \@subIns_fwd;
	my @subDel_fwd = (0,0,0,0);
        push @Del_fwd, \@subDel_fwd;
	my @subUnknown_fwd = (0);
        push @Unknown_fwd, \@subUnknown_fwd;
}



my $line=0;
my $i=0;
my @temp=();
my $length=0;

while($line = <FILE_A>){
	#print "$line";
	chomp($line);
	@temp=split("\t",$line);
	$A[$length][0] = $temp[0];
	$A[$length][1] = $temp[1];
	$A[$length][2] = $temp[2];
	$A[$length][3] = $temp[3];	
	#print "$A[$length][1]\t$A[$length][0]\t$A[$length][3]\t$A[$length][2]\n";
	$length = $length +1;
}


$length=0;
while($line = <FILE_A_fwd>){
        #print "$line";
        chomp($line);
        @temp=split("\t",$line);
        $A_fwd[$length][0] = $temp[0];
        $A_fwd[$length][1] = $temp[1];
        $A_fwd[$length][2] = $temp[2];
        $A_fwd[$length][3] = $temp[3];
        #print "$A_fwd[$length][1]\t$A_fwd[$length][0]\t$A_fwd[$length][3]\t$A_fwd[$length][2]\n";
        $length = $length +1;
}



$length=0;
while($line = <FILE_T>){
        chomp($line);
	@temp=split("\t",$line);
        $T[$length][0] = $temp[0];
        $T[$length][1] = $temp[1];
        $T[$length][2] = $temp[2];
        $T[$length][3] = $temp[3];
	#print "$T[$length][1]\t$T[$length][0]\t$T[$length][3]\t$T[$length][2]\n";
	$length = $length +1;
}


$length=0;
while($line = <FILE_T_fwd>){
        chomp($line);
        @temp=split("\t",$line);
        $T_fwd[$length][0] = $temp[0];
        $T_fwd[$length][1] = $temp[1];
        $T_fwd[$length][2] = $temp[2];
        $T_fwd[$length][3] = $temp[3];
        #print "$T_fwd[$length][0]\t$T_fwd[$length][1]\t$T_fwd[$length][2]\t$T_fwd[$length][3]\n";
        $length = $length +1;
}





$length=0;
while($line = <FILE_G>){
        chomp($line);
	@temp=split("\t",$line);
        $G[$length][0] = $temp[0];
        $G[$length][1] = $temp[1];
        $G[$length][2] = $temp[2];
        $G[$length][3] = $temp[3];
	$length = $length +1;
}


$length=0;
while($line = <FILE_G_fwd>){
        chomp($line);
        @temp=split("\t",$line);
        $G_fwd[$length][0] = $temp[0];
        $G_fwd[$length][1] = $temp[1];
        $G_fwd[$length][2] = $temp[2];
        $G_fwd[$length][3] = $temp[3];
        $length = $length +1;
}







$length=0;
while($line = <FILE_C>){
        chomp($line);
	@temp=split("\t",$line);
        $C[$length][0] = $temp[0];
        $C[$length][1] = $temp[1];
        $C[$length][2] = $temp[2];
        $C[$length][3] = $temp[3];
	$length = $length +1;
}

$length=0;
while($line = <FILE_C_fwd>){
        chomp($line);
        @temp=split("\t",$line);
        $C_fwd[$length][0] = $temp[0];
        $C_fwd[$length][1] = $temp[1];
        $C_fwd[$length][2] = $temp[2];
        $C_fwd[$length][3] = $temp[3];
        $length = $length +1;
}



$length=0;
while($line = <FILE_Ins>){
        chomp($line);
        @temp=split("\t",$line);
        $Ins[$length][0] = $temp[0];
        $Ins[$length][1] = $temp[1];
        $Ins[$length][2] = $temp[2];
        $Ins[$length][3] = $temp[3];
        $length = $length +1;
}

$length=0;
while($line = <FILE_Ins_fwd>){
        chomp($line);
        @temp=split("\t",$line);
        $Ins_fwd[$length][0] = $temp[0];
        $Ins_fwd[$length][1] = $temp[1];
        $Ins_fwd[$length][2] = $temp[2];
        $Ins_fwd[$length][3] = $temp[3];
        $length = $length +1;
}


$length=0;
while($line = <FILE_Del>){
        chomp($line);
        @temp=split("\t",$line);
        $Del[$length][0] = $temp[0];
        $Del[$length][1] = $temp[1];
        $Del[$length][2] = $temp[2];
        $Del[$length][3] = $temp[3];
        $length = $length +1;
}

$length=0;
while($line = <FILE_Del_fwd>){
        chomp($line);
        @temp=split("\t",$line);
        $Del_fwd[$length][0] = $temp[0];
        $Del_fwd[$length][1] = $temp[1];
        $Del_fwd[$length][2] = $temp[2];
        $Del_fwd[$length][3] = $temp[3];
        $length = $length +1;
}



$length=0;
while($line = <FILE_Unknown>){
	#print $line;
        chomp($line);
        $Unknown[$length][0] = $line;
        $length = $length +1;
}

$length=0;
while($line = <FILE_Unknown_fwd>){
        #print $line;
	chomp($line);
	@temp=split("\t",$line);
	#print $temp[0]."\n";
        $Unknown_fwd[$length][0] = $temp[0];
        $length = $length +1;
}




for($i=0; $i<$length1;$i++){
	#print "$T[$i][1]\t$T[$i][0]\t$T[$i][3]\t$T[$i][2]\n";
	#print "$T_fwd[$i][1]\t$T_fwd[$i][0]\t$T_fwd[$i][3]\t$T_fwd[$i][2]\n";
	print $FILE_A_all "${\($T[$length1-1-$i][1]+$A_fwd[$i][0])}\t${\($T[$length1-1-$i][0]+$A_fwd[$i][1])}\t${\($T[$length1-1-$i][3]+$A_fwd[$i][2])}\t${\($T[$length1-1-$i][2]+$A_fwd[$i][3])}\n"; 
	print $FILE_T_all "${\($A[$length1-1-$i][1]+$T_fwd[$i][0])}\t${\($A[$length1-1-$i][0]+$T_fwd[$i][1])}\t${\($A[$length1-1-$i][3]+$T_fwd[$i][2])}\t${\($A[$length1-1-$i][2]+$T_fwd[$i][3])}\n";
	print $FILE_G_all "${\($C[$length1-1-$i][1]+$G_fwd[$i][0])}\t${\($C[$length1-1-$i][0]+$G_fwd[$i][1])}\t${\($C[$length1-1-$i][3]+$G_fwd[$i][2])}\t${\($C[$length1-1-$i][2]+$G_fwd[$i][3])}\n";
	print $FILE_C_all "${\($G[$length1-1-$i][1]+$C_fwd[$i][0])}\t${\($G[$length1-1-$i][0]+$C_fwd[$i][1])}\t${\($G[$length1-1-$i][3]+$C_fwd[$i][2])}\t${\($G[$length1-1-$i][2]+$C_fwd[$i][3])}\n";

	print $FILE_Ins_all "${\($Ins[$length1-1-$i][1]+$Ins_fwd[$i][0])}\t${\($Ins[$length1-1-$i][0]+$Ins_fwd[$i][1])}\t${\($Ins[$length1-1-$i][3]+$Ins_fwd[$i][2])}\t${\($Ins[$length1-1-$i][2]+$Ins_fwd[$i][3])}\n";
	print $FILE_Del_all "${\($Del[$length1-1-$i][1]+$Del_fwd[$i][0])}\t${\($Del[$length1-1-$i][0]+$Del_fwd[$i][1])}\t${\($Del[$length1-1-$i][3]+$Del_fwd[$i][2])}\t${\($Del[$length1-1-$i][2]+$Del_fwd[$i][3])}\n";
	print $FILE_Unknown_all "${\($Unknown[$length1-1-$i][0]+$Unknown_fwd[$i][0])}\n";
}



close($FILE_A_all);
close($FILE_T_all);
close($FILE_G_all);
close($FILE_C_all);
close($FILE_Ins_all);
close($FILE_Del_all);
close($FILE_Unknown_all);

close(FILE_A_fwd);
close(FILE_T_fwd);
close(FILE_G_fwd);
close(FILE_C_fwd);
close(FILE_Ins_fwd);
close(FILE_Del_fwd);
close(FILE_Unknown_fwd);

close(FILE_A);
close(FILE_T);
close(FILE_G);
close(FILE_C);
close(FILE_Ins);
close(FILE_Del);
close(FILE_Unknown);



#!/usr/bin/perl
#Usage: ./sid_v0.x.pl R1.fwd.samCalmd Read_Length R1.errorA.txt R1.errorT.txt R1.errorG.txt R1.errorC.txt R1.errorINS.txt R1.errorDEL.txt R1.unknownNuc.txt R1.qualSubA.txt R1.qualSubT.txt R1.qualSubG.txt R1.qualSubC.txt R1.qualIns.txt R1.qualDel.txt R1.qualN.txt

#print "Started sid.pl ...\n";

use feature qw(switch);
use Scalar::Util qw(looks_like_number);
use strict; use warnings; use diagnostics;
use Switch;


my $sid_file = $ARGV[0];
#print "$sid_file\n";
my $length=$ARGV[1];
my $errorA=$ARGV[2];
my $errorT=$ARGV[3];
my $errorG=$ARGV[4];
my $errorC=$ARGV[5];
my $errorINS=$ARGV[6];
my $errorDEL=$ARGV[7];
my $errorN=$ARGV[8];
my $qualSubA=$ARGV[9];
my $qualSubT=$ARGV[10];
my $qualSubG=$ARGV[11];
my $qualSubC=$ARGV[12];
my $qualIns=$ARGV[13];
my $qualDel=$ARGV[14];
my $qualN=$ARGV[15];


#print "Initialising error matrices ... \n";

# A -> T -> G -> C
my @sub_array_A = (0,0,0,0);
my @array_A = (\@sub_array_A);

my @sub_array_C = (0,0,0,0);
my @array_C = (\@sub_array_C);

my @sub_array_G = (0,0,0,0);
my @array_G = (\@sub_array_G);

my @sub_array_T = (0,0,0,0);
my @array_T = (\@sub_array_T);

for(my $i=1; $i<$length; $i++){
	my @sub_array_A = (0,0,0,0);
	push @array_A, \@sub_array_A;
	
	my @sub_array_C = (0,0,0,0);
        push @array_C, \@sub_array_C;

	my @sub_array_G = (0,0,0,0);
        push @array_G, \@sub_array_G;

	my @sub_array_T = (0,0,0,0);
        push @array_T, \@sub_array_T;
}

my @array = (\@array_A,\@array_T,\@array_G,\@array_C);

#insertions & deletions
my @sub_array_ins = (0,0,0,0);
my @array_ins = (\@sub_array_ins);

my @sub_array_del = (0,0,0,0);
my @array_del = (\@sub_array_del);

for(my $i=1; $i<$length; $i++){
	my @sub_array_ins = (0,0,0,0);
	push @array_ins, \@sub_array_ins;
	
	my @sub_array_del = (0,0,0,0);
	push @array_del, \@sub_array_del;
}

#unknown nucleotides on read
my @array_N = (0);

for(my $i=1; $i<$length; $i++){
	push @array_N, 0;
}


#print "Initialise qual matrices ...\n";

my $maxQual = 50;

# A -> T -> G -> C
my @sub_qual_A = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualA = (\@sub_qual_A);

my @sub_qual_C = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualC = (\@sub_qual_C);

my @sub_qual_G = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualG = (\@sub_qual_G);

my @sub_qual_T = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualT = (\@sub_qual_T);

for(my $i=1; $i<$length; $i++){
        my @sub_qual_A = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualA, \@sub_qual_A;

        my @sub_qual_C = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualC, \@sub_qual_C;

        my @sub_qual_G = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualG, \@sub_qual_G;

        my @sub_qual_T = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualT, \@sub_qual_T;
}

my @arrayQual = (\@array_qualA,\@array_qualT,\@array_qualG,\@array_qualC);

#insertions & deletions
my @sub_qual_ins = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualIns = (\@sub_qual_ins);

my @sub_qual_del = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualDel = (\@sub_qual_del);

for(my $i=1; $i<$length; $i++){
        my @sub_qual_ins = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualIns, \@sub_qual_ins;

        my @sub_qual_del = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualDel, \@sub_qual_del;
}

#unknown nucleotides on read
my @sub_qual_N = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @array_qualN = (\@sub_qual_N);

for(my $i=1; $i<$length; $i++){
	my @sub_qual_N = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @array_qualN, \@sub_qual_N;
}







#print "Initialising temp help error matrices ... \n";

# A -> T -> G -> C
my @h_sub_array_A = (0,0,0,0);
my @h_array_A = (\@h_sub_array_A);

my @h_sub_array_C = (0,0,0,0);
my @h_array_C = (\@h_sub_array_C);

my @h_sub_array_G = (0,0,0,0);
my @h_array_G = (\@h_sub_array_G);

my @h_sub_array_T = (0,0,0,0);
my @h_array_T = (\@h_sub_array_T);

for(my $i=1; $i<$length; $i++){
	my @h_sub_array_A = (0,0,0,0);
	push @h_array_A, \@h_sub_array_A;
	
	my @h_sub_array_C = (0,0,0,0);
	push @h_array_C, \@h_sub_array_C;
	
	my @h_sub_array_G = (0,0,0,0);
	push @h_array_G, \@h_sub_array_G;
	
	my @h_sub_array_T = (0,0,0,0);
	push @h_array_T, \@h_sub_array_T;
}

my @h_array = (\@h_array_A,\@h_array_T,\@h_array_G,\@h_array_C);

#insertions & deletions help array
my @h_sub_array_ins = (0,0,0,0);
my @h_array_ins = (\@h_sub_array_ins);

my @h_sub_array_del = (0,0,0,0);
my @h_array_del = (\@h_sub_array_del);

for(my $i=1; $i<$length; $i++){
	my @h_sub_array_ins = (0,0,0,0);
	push @h_array_ins, \@h_sub_array_ins;
	
	my @h_sub_array_del = (0,0,0,0);
	push @h_array_del, \@h_sub_array_del;
}


#unknown nucleotides on read
my @h_array_N = (0);

for(my $i=1; $i<$length; $i++){
	push @h_array_N, 0;
}


#print "Initialise qual matrices ...\n";

# A -> T -> G -> C
my @h_sub_qual_A = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualA = (\@h_sub_qual_A);

my @h_sub_qual_C = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualC = (\@h_sub_qual_C);

my @h_sub_qual_G = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualG = (\@h_sub_qual_G);

my @h_sub_qual_T = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualT = (\@h_sub_qual_T);

for(my $i=1; $i<$length; $i++){
        my @h_sub_qual_A = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualA, \@h_sub_qual_A;

        my @h_sub_qual_C = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualC, \@h_sub_qual_C;

        my @h_sub_qual_G = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualG, \@h_sub_qual_G;

        my @h_sub_qual_T = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualT, \@h_sub_qual_T;
}

my @h_arrayQual = (\@h_array_qualA,\@h_array_qualT,\@h_array_qualG,\@h_array_qualC);

#insertions & deletions
my @h_sub_qual_ins = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualIns = (\@h_sub_qual_ins);

my @h_sub_qual_del = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualDel = (\@h_sub_qual_del);

for(my $i=1; $i<$length; $i++){
        my @h_sub_qual_ins = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualIns, \@h_sub_qual_ins;

        my @h_sub_qual_del = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualDel, \@h_sub_qual_del;
}

#unknown nucleotides on read
my @h_sub_qual_N = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @h_array_qualN = (\@h_sub_qual_N);

for(my $i=1; $i<$length; $i++){
	my @h_sub_qual_N = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        push @h_array_qualN, \@h_sub_qual_N;
}





#Look for MD tag

my $MD = "MD:Z:";
#print "\nMD=$MD \n\n";

open(my $FILE_Calmd, $sid_file) or die "Can't open $sid_file\n";

#create a file for lines where an error occurred 
#open(my $FILE_badLines, '>', "badLines.txt") or die "Can't open badLines.txt\n";

#open files for error matrices
#files for A, C, G and T
open(my $FILE_A, '>', $errorA) or die "Can't open errorMatrix_A.txt\n";
open(my $FILE_C, '>', $errorC) or die "Can't open errorMatrix_C.txt\n";
open(my $FILE_G, '>', $errorG) or die "Can't open errorMatrix_G.txt\n";
open(my $FILE_T, '>', $errorT) or die "Can't open errorMatrix_T.txt\n";
open(my $FILE_INS, '>', $errorINS) or die "Can't open errorMatrix_INS.txt\n";
open(my $FILE_DEL, '>', $errorDEL) or die "Can't open errorMatrix_DEL.txt\n";
open(my $FILE_N, '>', $errorN) or die "Can't open unknownNuc.txt\n";

#open files for qual matrices
open(my $FILE_Q_A, '>', $qualSubA) or die "Can't open qualMatrix_A.txt\n";
open(my $FILE_Q_C, '>', $qualSubC) or die "Can't open qualMatrix_C.txt\n";
open(my $FILE_Q_G, '>', $qualSubG) or die "Can't open qualMatrix_G.txt\n";
open(my $FILE_Q_T, '>', $qualSubT) or die "Can't open qualMatrix_T.txt\n";
open(my $FILE_Q_INS, '>', $qualIns) or die "Can't open qualMatrix_INS.txt\n";
open(my $FILE_Q_DEL, '>', $qualDel) or die "Can't open qualMatrix_DEL.txt\n";
open(my $FILE_Q_N, '>', $qualN) or die "Can't open qual unknownNuc.txt\n";

#keep track of the line number
my $count_line=0;

#lines to read-in from read file
my $line=0;

my @HelpLineSplit = ();
my $Cigar = 0;
my $Cigar2 = 0;
my $MDtag = 0;
my $MDtagExtended = 0;

my @CigarSplit = ();
my $lastChar = 0;
my $CigarNr = 0;
my $CigarExtended = 0;

my $qualString = 0;
my @qualArray = (0);
for(my $k=1;$k<$length;$k++){
	push @qualArray, 0;
}
my $tempQual=0;
my $offset=33;

my $i = 0;
my $j = 0;
my $k = 0;
	my $read_counter=0;
	my $temp=0;
	my $nuc=-1;
	my $nuc_read=-1;
	my $nuc_ref=-1;

	########################################
	########################################
	########################################

	#seek FILE, 0, 0;

	my $indLength=0;

	#start reading in reads
	while($line = <$FILE_Calmd>){
		
		#print("\n\n--------------------\n\n$line\n\n");
		#print("\n\n--------------------\n\n");	

		#reset help arrays
		for($i=0; $i<4; $i++){
			for($j=0; $j<$length; $j++){
				for($k=0; $k<4; $k++){
					$h_array[$i][$j][$k]=0;
				}
				for($k=0; $k<$maxQual; $k++){
					$h_arrayQual[$i][$j][$k]=0;
				}
			}
		}
		
		for($j=0; $j<$length; $j++){
			$h_array_N[$j]=0;
			for($k=0; $k<4; $k++){
				$h_array_ins[$j][$k]=0;
				$h_array_del[$j][$k]=0;
			}
			for($k=0; $k<$maxQual; $k++){
				$h_array_qualIns[$j][$k]=0;
				$h_array_qualDel[$j][$k]=0;
				$h_array_qualN[$j][$k]=0;
			}
		}
		
		
		#Reset variables
		$Cigar = 0;
		$Cigar2 = 0;
		$MDtag = 0;
		$qualString = 0;
		for($k=0;$k<$length;$k++){
			$qualArray[$k]=0;
		}
		$indLength=0;
		
		#print $Cigar."\t".$Cigar2."\n";
		
		#ignore headers
		while($line =~ m/^@/){
			$line = <$FILE_Calmd>;
			$count_line++;
		}
		
		#increment line counter
		$count_line++;
		
		#print "$count_line\n";
		
		#update on progress
		if(($count_line % 10000) == 0){
			print ">>> processing line $count_line\t";
		}
		
		chomp($line);
		#print "$line \n";
		
		@HelpLineSplit = split(/\t/,$line);

		#Save qual scores in string
		$qualString =  $HelpLineSplit[10]; 
		#print "qual String:\t$qualString\n";	
		
		if($qualString eq '*'){
			next;
		}
		#print "$qualString\n";

		$indLength=length($qualString);

		#print "Read length: ".$indLength."\n";

		for($k=0;$k<$indLength;$k++){
				$tempQual = substr $qualString, $k, 1;
				$qualArray[$k]=ord($tempQual)-$offset;
				#print "$qualArray[$k],";
		}

		
		#print CIGAR string
		$Cigar = $HelpLineSplit[5];
		$Cigar2 = $HelpLineSplit[9];

		#print "Cigar and Cigar2:\n".$Cigar."\t".$Cigar2."\n\n\n";
		
		
		#extent Cigar string
		#first split Cigar string by M, D and I
		$Cigar =~ s/M/M\t/g;
		$Cigar =~ s/D/D\t/g;
		$Cigar =~ s/I/I\t/g;
		$Cigar =~ s/S/S\t/g;
		
		#print $Cigar."\t".$MDtag."\n\n\n";
		
		@CigarSplit = split(/\t/,$Cigar);
		
		#check splitting went ok
		#foreach(@CigarSplit){
		#	print "$_\n";
		#}
		
		
		$CigarExtended=();
		$i=0;
		while(defined $CigarSplit[$i]){
			$lastChar=substr $CigarSplit[$i],-1,1;
			$CigarNr= substr $CigarSplit[$i],0,length($CigarSplit[$i])-1;
			
			#print "\n".$CigarNr."\n";
			
			switch ($lastChar) {
				case "M"	{ 
					for($j=0;$j<$CigarNr;$j++){
						#print "M";
						$CigarExtended = $CigarExtended."M";
					}
				}
				case "I"	{
					for($j=0;$j<$CigarNr;$j++){
						#print "I";
						$CigarExtended = $CigarExtended."I";
					}
				}
				case "D"	{
					for($j=0;$j<$CigarNr;$j++){
						#print "D";
						$CigarExtended = $CigarExtended."D";
					}
				}
				else { print "problem in switch statement"; exit (0);}
			}
			
			$i++;
		}
		#print "\n";
		
		#print $CigarExtended."\n".$Cigar2."\n";
		
		
		###########
		## Extend MD tag
		###########
		
		#check if MD tag is present (field number can vary)
		$i=0;
		while((defined $HelpLineSplit[$i])){
			#print "$helpSplit[$i]\n";
			$temp = substr($HelpLineSplit[$i],0,5);
			
			if((defined $temp) && ($temp eq $MD)){
				$MDtag=substr($HelpLineSplit[$i],5,length($HelpLineSplit[$i]));
				last;			
			}
			else{	
				$i++;
			}
		}
		#print $MDtag."\n\n";

		#No MD tag or perfect match
		if(($MDtag eq $indLength) && ($CigarNr eq $indLength)){
			next;
		}
		
		#Possible symbols in MD tag: number, [A,C,T,G], ^, N,K,R,Y,M,S,W
		$MDtagExtended=();
		$temp=0;
		while($MDtag ne ''){
			if($MDtag =~ m/^(\d+)/){
				$temp=$1;
				for($i=0;$i<$temp;$i++){
					$MDtagExtended=$MDtagExtended."=";
				}
				$MDtag =~ s/^(\d+)//;
			}
			else{
				if($MDtag =~ m/^([A,T,G,C]+)/){
					$temp=$1;
					$MDtagExtended=$MDtagExtended.$temp;
					$MDtag =~ s/^[A,T,G,C]+//;
				}
				else{
					if($MDtag =~ m/^(\^)/){
						$temp=$1;
						$MDtagExtended=$MDtagExtended.$temp;
						$MDtag =~ s/^(\^)//;
					}
					else{
						if($MDtag =~ m/^N/){		#corresponds to an N on the reference sequence -> read can have any nucleotide at this position
							$temp=$1;
							$MDtagExtended=$MDtagExtended."N";
							$MDtag =~ s/^N//;
						}
						else{
							if($MDtag =~ m/^([N,K,R,Y,M,S,W]+)/){
							$temp=$1;
							$MDtagExtended=$MDtagExtended."X";
							$MDtag =~ s/^[N,K,R,Y,M,S,W]+//;
						}
						else{
							print "Problem with MDtag: ".$MDtag."\n";
							print $line."\n";
							last;
						}
					}
				}
			}
		}
	}
	
	#print $MDtagExtended."\n\n";
	

	
	#######################################
	#Compare:	CigarExtended, Cigar2, MDtagExtended
	#	CigarExtended and Cigar2 for Insertion on the read
	#	CigarExtended and MDtagExtended for deletions on the read
	
	
	#need a loop variable for each string
	
	
	$i=0;
	$j=0;
	$k=0;	
	$read_counter=0;

	#printf "CigarExtended=$CigarExtended\n";
	#printf "Cigar2=$Cigar2\n";
	#printf "MDtagExtended=$MDtagExtended\n";
	
	while(($i<length($CigarExtended)) && ($j<length($Cigar2)) && ($k<length($MDtagExtended))){

		$nuc=-1;
		$nuc_read=-1;
		$nuc_ref=-1;
		
		#if a match is encounter, we do nothing
		if((substr($CigarExtended,$i,1) eq "M") && (substr($Cigar2,$j,1) eq "=") && (substr($MDtagExtended,$k,1) eq "=")){ 
			$i++;
			$j++;
			$k++;
			$read_counter++;
		}
		else{
			
			switch(substr($CigarExtended,$i,1)){
				#if an insertion on the read is encountered
				case "I"	{
					if(substr($Cigar2,$j,1) eq "N"){
						$h_array_N[$read_counter]++;
						$h_array_qualN[$read_counter][($qualArray[$read_counter])-1]++;
						#print "\n\n>>>>>>".$h_array_N[$read_counter]."\n";
					}
					else{
						switch(substr($Cigar2,$j,1)){
							case "A"	{
								$nuc=0;
							}
							case "T"	{
								$nuc=1;
							}
							case "G"	{
								$nuc=2;
							}
							case "C"	{
								$nuc=3;
							}
						}
						$h_array_ins[$read_counter][$nuc]++;
						#record qual score for insertion
						$h_array_qualIns[$read_counter][($qualArray[$read_counter])-1]++;
					}
					
					$i++;
					$j++;
					$read_counter++;
				}
				case "D"	{
					
					if(substr($MDtagExtended,$k,1) eq "^"){
						$k++;
					}

						if(substr($MDtagExtended,$k,1) eq "N"){			#if the reference shows an "N" at this positon, we do nothing
						}
						else{
							switch(substr($MDtagExtended,$k,1)){
								case "A"	{
									$nuc=0;
								}
								case "T"	{
									$nuc=1;
								}
								case "G"	{
									$nuc=2;
								}
								case "C"	{
									$nuc=3;
								}
									
							}
							$h_array_del[$read_counter][$nuc]++;
							#record qual score for deltion
							$h_array_qualDel[$read_counter][($qualArray[$read_counter])-1]++;
						}
					
					$i++;
					$k++;
				}
				case "N"	{		#treat N (skipped reference) as a deletion
					
					if(substr($MDtagExtended,$k,1) eq "^"){
						$k++;
					}
					else{
						if(substr($MDtagExtended,$k,1) eq "N"){			#if the reference shows an "N" at this positon, we do nothing
						}
						else{
							switch(substr($MDtagExtended,$k,1)){
								case "A"	{
									$nuc=0;
								}
								case "T"	{
									$nuc=1;
								}
								case "G"	{
									$nuc=2;
								}
								case "C"	{
									$nuc=3;
								}
							}
							$h_array_del[$read_counter][$nuc]++;
							#record qual score for N
							$h_array_qualDel[$read_counter][($qualArray[$read_counter])-1]++;
						}
					}
					$i++;
					$k++;
				}
				case "M"	{
					
					if(substr($MDtagExtended,$k,1) eq "N"){			#if the reference shows an "N" at this positon, we do nothing
					}
					else{
						if(substr($Cigar2,$j,1) eq "N"){				#if the read shows an N
							$h_array_N[$read_counter]++;
							$h_array_qualN[$read_counter][($qualArray[$read_counter])-1]++;
						}
						else{
							if(substr($MDtagExtended,$k,1) eq "X"){
							}
							else{
								switch(substr($Cigar2,$j,1)){
									case "A"	{
										$nuc_read=0;
									}
									case "T"	{
										$nuc_read=1;
									}
									case "G"	{
										$nuc_read=2;
									}
									case "C"	{
										$nuc_read=3;
									}
								}
								switch(substr($MDtagExtended,$k,1)){
									case "A"	{
										$nuc_ref=0;
									}
									case "T"	{
										$nuc_ref=1;
									}
									case "G"	{
										$nuc_ref=2;
									}
									case "C"	{
										$nuc_ref=3;
									}
								}
								if($nuc_read == $nuc_ref){
									print $CigarExtended."\n".$Cigar2."\n";	
									print $MDtagExtended."\n\n";
								}
								$h_array[$nuc_ref][$read_counter][$nuc_read]++;
								$h_arrayQual[$nuc_ref][$read_counter][($qualArray[$read_counter])-1]++;
							}
						}
					}
					$i++;
					$j++;
					$k++;
					$read_counter++;
					
				}
				else	{
					#print $FILE_badLines "$line \n";
					#print $FILE_badLines substr($CigarExtended,$i,1)."\t";
					#print $FILE_badLines substr($Cigar2,$j,1)."\t";
					#print $FILE_badLines substr($MDtagExtended,$k,1)."\n";
					#print $FILE_badLines "---------------------------------------\n\n";
					
					last;
					
				}
				
			}
			
			
		}
			
	}
	

	
	#add help_array to array 
	for($i=0; $i<4; $i++){
		for($j=0; $j<$indLength; $j++){
			for($k=0; $k<4; $k++){
				$array[$i][$j][$k]=$array[$i][$j][$k]+$h_array[$i][$j][$k];
				#print $array[$i][$j][$k]."\t";
			}
		#print "\n";
		}
		#print "---------------------\n";
	}
	
	for($j=0; $j<$indLength; $j++){
		$array_N[$j]=$array_N[$j]+$h_array_N[$j];
		for($k=0; $k<4; $k++){
			$array_ins[$j][$k]=$array_ins[$j][$k]+$h_array_ins[$j][$k];
			$array_del[$j][$k]=$array_del[$j][$k]+$h_array_del[$j][$k];
		}
	}

	#do the same for qual score arrays
	for($i=0; $i<4; $i++){
                for($j=0; $j<$indLength; $j++){
                        for($k=0; $k<$maxQual; $k++){
                                $arrayQual[$i][$j][$k]=$arrayQual[$i][$j][$k]+$h_arrayQual[$i][$j][$k];
                                #print $array[$i][$j][$k]."\t";
                        }
                #print "\n";
                }
                #print "---------------------\n";
        }
	
	for($j=0; $j<$indLength; $j++){
                for($k=0; $k<$maxQual; $k++){
                        $array_qualIns[$j][$k]=$array_qualIns[$j][$k]+$h_array_qualIns[$j][$k];
                        $array_qualDel[$j][$k]=$array_qualDel[$j][$k]+$h_array_qualDel[$j][$k];
			$array_qualN[$j][$k]=$array_qualN[$j][$k]+$h_array_qualN[$j][$k];
                }
        }
      

	#for($i=0; $i<4; $i++){
        #        for($j=0; $j<$length; $j++){
        #                #for($k=0; $k<$maxQual; $k++){
        # 		print "$arrayQual[$i][$j][0]\t$arrayQual[$i][$j][1]\n";
			#}
	#	}
	#	print "-------------\n";
	#}	

	
	#end of while loop	
	

}


print "\n\n";




######################################################
######################################################
######################################################








#print "Error matrix for A:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	print $FILE_A "$array[0][$k][0]\t";
	print $FILE_A "$array[0][$k][1]\t";
	print $FILE_A "$array[0][$k][2]\t";
	print $FILE_A "$array[0][$k][3]\n";
	#print to screen
#	print "$k \t $array[0][$k][0] \t";
#	print "$array[0][$k][1] \t";
#	print "$array[0][$k][2] \t";
#	print "$array[0][$k][3] \n";
} 

#print "\n\n";

#print "Error matrix for T:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
        print $FILE_T "$array[1][$k][0]\t";
        print $FILE_T "$array[1][$k][1]\t";
        print $FILE_T "$array[1][$k][2]\t";
        print $FILE_T "$array[1][$k][3]\n";
	#print to screen
#	print "$k \t $array[1][$k][0] \t";
#	print "$array[1][$k][1] \t";
#	print "$array[1][$k][2] \t";
#	print "$array[1][$k][3] \n";
}

#print "\n\n";

#print "Error matrix for G:\n\n"; 

for(my $k=0; $k<$length; $k++){
	#print to file
	print $FILE_G "$array[2][$k][0]\t";
        print $FILE_G "$array[2][$k][1]\t";
        print $FILE_G "$array[2][$k][2]\t";
        print $FILE_G "$array[2][$k][3]\n";
	#print to screen
#	print "$k \t $array[2][$k][0] \t";
#	print "$array[2][$k][1] \t";
#	print "$array[2][$k][2] \t";
#	print "$array[2][$k][3] \n";
}

#print "\n\n";

#print "Error matrix for C:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
        print $FILE_C "$array[3][$k][0]\t";
        print $FILE_C "$array[3][$k][1]\t";
        print $FILE_C "$array[3][$k][2]\t";
        print $FILE_C "$array[3][$k][3]\n";
	#print to screen
#	print "$k \t $array[3][$k][0] \t";
#	print "$array[3][$k][1] \t";
#	print "$array[3][$k][2] \t";
#	print "$array[3][$k][3] \n";
}

#print "\n\n";


#print "Error matrix for deletions:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	print $FILE_DEL "$array_del[$k][0]\t";
	print $FILE_DEL "$array_del[$k][1]\t";
	print $FILE_DEL "$array_del[$k][2]\t";
	print $FILE_DEL "$array_del[$k][3]\n";	
	#print to screen 
#	print "$k \t $array_del[$k][0] \t";
#	print "$array_del[$k][1] \t";
#	print "$array_del[$k][2] \t";
#	print "$array_del[$k][3] \n";
}

	
#print "\n\n";


#print "Error matrix for insertions:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	print $FILE_INS "$array_ins[$k][0]\t";
	print $FILE_INS "$array_ins[$k][1]\t";
	print $FILE_INS "$array_ins[$k][2]\t";
	print $FILE_INS "$array_ins[$k][3]\n";	
	#print to screen
#	print "$k \t $array_ins[$k][0] \t";
#	print "$array_ins[$k][1] \t";
#	print "$array_ins[$k][2] \t";
#	print "$array_ins[$k][3] \n";
}


for(my $k=0; $k<$length; $k++){
	#print to file
	print $FILE_N "$array_N[$k] \n";
	#print to screen
	#print "$k \t $array_N[$k] \n";
}



########
#print qual scores
########

#print "Qual matrix for A:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
		print $FILE_Q_A "$arrayQual[0][$k][$i]\t";
		#print to screen
		#print "$arrayQual[0][$k][$i] \t";
	}
        print $FILE_Q_A "\n";
	#print "\n";	
} 

#print "\n\n";

#print "Qual matrix for T:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
        	print $FILE_Q_T "$arrayQual[1][$k][$i]\t";
		#print to screen
		#print "$arrayQual[1][$k][$i] \t";
	}
        print $FILE_Q_T "\n";
	#print "\n";
}

#print "\n\n";

#print "Qual matrix for G:\n\n"; 

for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
		print $FILE_Q_G "$arrayQual[2][$k][$i]\t";
		#print to screen
		#print "$arrayQual[2][$k][$i] \t";
	}
        print $FILE_Q_G "\n";
	#print "\n";
}

#print "\n\n";

#print "Qual matrix for C:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
       		print $FILE_Q_C "$arrayQual[3][$k][$i]\t";
		#print to screen
		#print "$arrayQual[3][$k][$i] \t";
	}
        print $FILE_Q_C "\n";
	#print "\n";
}

#print "\n\n";


#print "Qual matrix for deletions:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
		print $FILE_Q_DEL "$array_qualDel[$k][$i]\t";
		#print to screen 
		#print "$array_qualDel[$k][$i] \t";
	}
        print $FILE_Q_DEL "\n";
	#print "\n";
}

	
#print "\n\n";


#print "Qual matrix for insertions:\n\n";

for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
		print $FILE_Q_INS "$array_qualIns[$k][$i]\t";
		#print to screen
		#print "$array_qualIns[$k][$i] \t";
	}
        print $FILE_Q_INS "\n";
	#print "\n";
}


for(my $k=0; $k<$length; $k++){
	#print to file
	for($i=0;$i<$maxQual;$i++){
		print $FILE_Q_N "$array_qualN[$k][$i]\t";
		#print to screen
		#print "$array_qualN[$k][$i] \t";
	}

	print $FILE_Q_N "\n";
	#print "\n";

}






close($FILE_Calmd);
#close($FILE_badLines);
close($FILE_A);
close($FILE_C);
close($FILE_G);
close($FILE_T);
close($FILE_DEL);
close($FILE_INS);
close($FILE_N);
close($FILE_Q_A);
close($FILE_Q_C);
close($FILE_Q_G);
close($FILE_Q_T);
close($FILE_Q_DEL);
close($FILE_Q_INS);
close($FILE_Q_N);




